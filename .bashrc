# -*- mode: shell-script -*-

# Quit if shell is non-interactive
[[ -z "$PS1" ]] && return

alias po=popd
alias pu=pushd
alias d=dirs
alias rm='rm -i --one-file-system'

case "$(hostname)" in
    arms-sic-*)
        function gate () {
            case "$1" in
                on)
                    echo -n -e "\x00\x01\x00\x01" > /proc/iguana/dio
                    ;;
                off)
                    echo -n -e "\x00\x00\x00\x01" > /proc/iguana/dio
                    ;;
                *)
                    echo "$((($(cat /proc/iguana/dio) & 0x0100) >> 8))"
                    ;;
            esac
        }
        alias gate-on='gate on'
        alias gate-off='gate off'
        alias engtest='readeng --debug ~/config/arms.cfg 2 /dev/null'
        alias advolts='readeng --volts --debug ~/config/arms.cfg 2 /dev/null'
        alias rdtcm='readtcm --debug /dev/ttyS1 2 /dev/null'
        ;;
    ivar-sic-*)
        alias engtest='readeng --debug ~/config/ivar.cfg 2 /dev/null'
        alias advolts='readeng --volts --debug ~/config/ivar.cfg 2 /dev/null'
        alias rdtcm='readtcm --debug /dev/ttyS1 2 /dev/null'
        ;;
esac
